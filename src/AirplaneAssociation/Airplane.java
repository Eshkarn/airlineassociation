/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AirplaneAssociation;

/**
 *
 * @author eshkarn
 */
public class Airplane {
    private String model;
    private String uniqueId;
    private int numberOfSeats;
    private String make;
    
    public Airplane(String model, String uniqueId, int numberOfSeats, String make)
    {
        this.model = model;
        this.uniqueId = uniqueId;
        this.numberOfSeats = numberOfSeats;
        this.make = make;
    }
    
    public String getUniqueId()
    {
        return this.uniqueId;
    }
    public int getNumberOfSeats()
    {
        return this.getNumberOfSeats();
        
    }
    public String getMake()
    {
        return this.make;
        
    }
    public String getModel()
    {
        return this.model;
    }
}
