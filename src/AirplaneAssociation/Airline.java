/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AirplaneAssociation;

/**
 *
 * @author eshkarn
 */
public class Airline {
    private String name;
    private String short_code;
    
    public Airline(String name, String short_code)
    {
        this.name = name;
        this.short_code = short_code;
       
    }
    
    public String getName()
    {
        return this.name;
    }
    
    public String getShortCode()
    {
        return this.short_code;
    }
    
}
